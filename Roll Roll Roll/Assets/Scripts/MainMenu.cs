﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	void Start(){
        
		if (GameObject.FindObjectsOfType <MainMenu>().Length > 1) {
			Destroy (this.gameObject);
		}

		SceneManager.sceneLoaded += LevelLoaded;
	}
		
		

	void OnEnable()
	{
		SceneManager.sceneLoaded += LevelLoaded;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= LevelLoaded;
	}

	void LevelLoaded(Scene scene,LoadSceneMode mode)
	{
		
		if (GameObject.Find ("BackButton") != null) {
			Debug.Log ("Back Button Found");
			Button backButton = GameObject.Find ("BackButton").GetComponent<Button> ();

			backButton.onClick.AddListener (() => {
				LoadMainMenu();
			});
		}

		if (GameObject.Find ("Credits") != null) {
			Debug.Log ("Credits Button Found");
			Button creditsButton = GameObject.Find ("Credits").GetComponent<Button> ();

			creditsButton.onClick.AddListener (() => {
				Debug.Log("credits");
				
			});
		}
	}


    public void PlayGame()
    {
		Debug.Log ("Play Game");
		SceneManager.LoadScene("Level_1");
       // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level_1");
    }

    public void LoadLevel2()
    {
        SceneManager.LoadScene("Level_2");
    }

    public void LoadLevel3()
    {
        SceneManager.LoadScene("Level_3");
    }


    public void QuitGame()
    {
        Application.Quit();
    }

}
