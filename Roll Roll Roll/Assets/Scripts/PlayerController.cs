﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public AudioClip bounceSound;
    public AudioClip starSound;
    public AudioClip redSound;
    public float speed;
    public float jump;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetScoreText();
        winText.text = "";
    }

    void FixedUpdate()
    {
        //movement
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void Update()
    {
        //jump and jump sound
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<AudioSource>().clip = bounceSound;
            GetComponent<AudioSource>().Play();

            Vector3 balljump = new Vector3(0.0f, jump, 0.0f);
            rb.AddForce(balljump * jump);
        }
        //ball falls off
        if (transform.position.y < -300)
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
            //Application.LoadLevel("");

        }

    }
    //changes jump height when on red platform
    void OnCollisionEnter(Collision ball)
    {
        if (ball.gameObject.name == "bounce")
        {
            jump = 80f;
            GetComponent<AudioSource>().clip = redSound;
            GetComponent<AudioSource>().Play();
        }
        else
        {
            jump = 35f;
        }
    }

    //score
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Pick Up"))
        {
            // star pick up sound
            GetComponent<AudioSource>().clip = starSound;
            GetComponent<AudioSource>().Play();
            // disables star
            col.gameObject.SetActive(false);
            //adds score
            count = count + 150;
            SetScoreText();          
        }
        //ends level
        if (col.gameObject.CompareTag("end"))
        {
            winText.text = "LEVEL COMPLETED   " + "SCORE: " + count.ToString();
            nextScene();
        }
        //boost
        if (col.gameObject.CompareTag("speed"))
        {
            speed = 100f;
            GetComponent<AudioSource>().clip = redSound;
            GetComponent<AudioSource>().Play();
        }
        else
        {
            speed = 50f;
        }
        //water
        if (col.gameObject.CompareTag("water"))
        {
            speed = 15f;
            jump = 25f;
            GetComponent<AudioSource>().clip = redSound;
            GetComponent<AudioSource>().Play();
        }
        else
        {
            speed = 50f;
            jump = 35f;
        }
    }

    void SetScoreText()
    {
        countText.text = "SCORE: " + count.ToString();
    }

    public void nextScene()
    {
        StartCoroutine("Wait");

    }
    // waits 3 seconds before switching scene
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        if (SceneManager.GetActiveScene().name == "Level_1")
        {
            SceneManager.LoadScene("Level_2");
        }
        if (SceneManager.GetActiveScene().name == "Level_2")
        {
            SceneManager.LoadScene("Level_3");
        }

        if (SceneManager.GetActiveScene().name == "Level_3")
        {
            SceneManager.LoadScene("menu");
        }

    }

}