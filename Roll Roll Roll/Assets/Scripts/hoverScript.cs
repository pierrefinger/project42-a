﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hoverScript : MonoBehaviour {

    public AudioSource mySound;
    public AudioClip hoverSound;


    public void HoverSound()
    {
        mySound.PlayOneShot(hoverSound);
    }
}
