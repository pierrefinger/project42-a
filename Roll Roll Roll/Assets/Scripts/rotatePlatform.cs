﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatePlatform : MonoBehaviour {

    private float speed = 0.5f;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {


        // ...also rotate around the World's Y axis ///transform.Rotate(Vector3.up * Time.deltaTime, speed);
        transform.Rotate(Vector3.up * speed, Space.World);
    }
}
