﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class starRotate : MonoBehaviour {

    public float speed = 30f;

    public GameObject destroyedVersion;


    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.name == "ball")
        {
            Debug.Log("boom");
            Instantiate(destroyedVersion, transform.position, transform.rotation);
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, speed * Time.deltaTime);
    }
}
